# TP Agilité

Groupe :
- REMOND Arthur
- ROYER Quentin


Techno :
- Typescript
- Jest 


# Install 
```bash
npm install
```

# Start Test

In *jest.config.json* write the name on your file test 
```js
 "testMatch": [
    "**/test_filename.spec.ts"
  ],
```
if you want run all test, you can write
```js
 "testMatch": [
    "**/*.spec.ts"
  ],
```
start test
```bash
npm run test
```
