import {Survivor} from "../src/zombie/Survivor"
import {Game} from "../src/zombie/Game"
import {Equipment} from "../src/zombie/Equipment"
import {Position} from "../src/zombie/Enum/Position";
import {Level} from "../src/zombie/Enum/Level";

describe('Survivor tests', () => {

    //STEP 1
    test('Survivor has a name', () => {
        let survivor1 = new Survivor('Arthur')
        expect(survivor1.name).toBe('Arthur')
    })

    test('Survivor dies immediately when getting 2 wounds', () => {
        let survivor1 = new Survivor('Arthur')
        expect(survivor1.alive).toBe(true)
        survivor1.addWounds(2)
        expect(survivor1.alive).toBe(false)
    })

    test('Survivor has 3 Actions per turn', () => {
        let survivor1 = new Survivor('Arthur')
        let game1 = new Game()

        game1.addSurvivor(survivor1)

        survivor1.useAction()
        survivor1.useAction()
        survivor1.useAction()

        let testAction = survivor1.useAction()
        expect(survivor1.actions).toBe(0)

        expect(game1.turn).toBe(1)

        let isTurnChanged = game1.changeTurn()
        expect(isTurnChanged).toBe(true)
        expect(game1.turn).toBe(2)


        expect(testAction).toBe(false)

        let areActionsReseted = survivor1.actions
        expect(areActionsReseted).toBe(3)
    })

    //STEP 2
    test('Survivor equips 5 pieces of stuff', () => {
        let survivor1 = new Survivor('Arthur')

        let result = survivor1.addEquipment(new Equipment('Baseball bat'))
        expect(result).toBe(true)
        result = survivor1.addEquipment(new Equipment('Katana'))
        expect(result).toBe(true)
        result = survivor1.addEquipment(new Equipment('Pistol'))
        expect(result).toBe(true)
        result = survivor1.addEquipment(new Equipment('Bottled Water'))
        expect(result).toBe(true)
        result = survivor1.addEquipment(new Equipment('Molotov'))
        expect(result).toBe(true)

        result = survivor1.addEquipment(new Equipment('Frying pan'))
        expect(result).toBe(false)

        let leftHandequipement = survivor1.getHand(Position.Left)
        let rightHandequipement = survivor1.getHand(Position.Right)
        expect(leftHandequipement).not.toBeNull()
        expect(rightHandequipement).not.toBeNull()
    })

    test('Survivor equips 5 pieces of stuff', () => {
        let survivor1 = new Survivor('Arthur')

        survivor1.addEquipment(new Equipment('Baseball bat'))
        survivor1.addEquipment(new Equipment('Katana'))
        survivor1.addEquipment(new Equipment('Pistol'))
        survivor1.addEquipment(new Equipment('Bottled Water'))
        survivor1.addEquipment(new Equipment('Molotov'))

        expect(survivor1.reserveSize).toBe(3)
        survivor1.getHurt()
        expect(survivor1.reserveSize).toBe(2)
    })

    test('Switch equipment', () => {
        let survivor1 = new Survivor('Arthur')

        survivor1.addEquipment(new Equipment('Baseball bat'))
        survivor1.addEquipment(new Equipment('Katana'))
        survivor1.addEquipment(new Equipment('Pistol'))
        survivor1.addEquipment(new Equipment('Bottled Water'))
        survivor1.addEquipment(new Equipment('Molotov'))

        expect(survivor1.getHand(Position.Left).name).toBe('Baseball bat')
        expect(survivor1.getReserveEquipment(0).name).toBe('Pistol')
        survivor1.switchEquipment(Position.Left, 0)
        expect(survivor1.getHand(Position.Left).name).toBe('Pistol')
        expect(survivor1.getReserveEquipment(0).name).toBe('Baseball bat')
    })


    //STEP 3
    test('Game begins with 0 survivor', () => {
        let game1 = new Game()
        expect(game1.isRunning()).toBe(true)
    })

    test('add unique survivor during game', () => {
        let game1 = new Game()

        let survivor1 = new Survivor('Arthur')
        let survivor2 = new Survivor('Quentin')

        let addSurvivorDuringGame = game1.addSurvivor(survivor1)
        expect(addSurvivorDuringGame).toBe(true)

        addSurvivorDuringGame = game1.addSurvivor(survivor2)
        expect(addSurvivorDuringGame).toBe(true)

        addSurvivorDuringGame = game1.addSurvivor(survivor1)
        expect(addSurvivorDuringGame).toBe(false)
    })

    test('Game ends immediatly when all survivor have died', () => {
        let game1 = new Game()

        let survivor1 = new Survivor('Arthur')
        let survivor2 = new Survivor('Quentin')

        game1.addSurvivor(survivor1)
        game1.addSurvivor(survivor2)

        survivor1.getHurt()
        survivor1.getHurt()
        expect(game1.isRunning()).toBe(true)
        survivor2.getHurt()
        survivor2.getHurt()
        expect(game1.isRunning()).toBe(false)
    })

    //STEP4
    test('Survivor starts with level 1', () => {
        let survivor1 = new Survivor('Arthur')
        expect(survivor1.experience).toBe(0)
    })

    test('Survivor starts with Level exp', () => {
        let survivor1 = new Survivor('Arthur')
        expect(survivor1.level).toBe(Level.Blue)
    })

})

