import {TennisGame} from "../../src/tennis/TennisGame";
import {Score} from "../../src/tennis/Score";

describe('test ', () => {

    test('test casual score', () => {

        const points = [0, 1, 0, 0]

        let game1 = new TennisGame()
        points.forEach(point => game1.add(point))

        const score = game1.getScore();
        let joueur1 = score[0];
        let joueur2= score[1];

        expect(joueur1).toBe(Score.Fourty);
        expect(joueur2).toBe(Score.Fifteen);

    });

    test('score both getting 40A', () => {

        const points = [0, 1, 0, 1, 0, 1]

        let game1 = new TennisGame()
        points.forEach(point => game1.add(point))

        const score = game1.getScore();
        let joueur1 = score[0];
        let joueur2 = score[1];

        expect(joueur1).toBe(Score.FourtyAdvantage);
        expect(joueur2).toBe(Score.FourtyAdvantage);
    });

    test('score both getting 40A and one scoring', () => {

        const points = [0, 1, 0, 1, 0, 1, 0]

        let game1 = new TennisGame()
        points.forEach(point => game1.add(point))

        const score = game1.getScore();
        let joueur1 = score[0];
        let joueur2 = score[1];

        expect(joueur1).toBe(Score.Advantage);
        expect(joueur2).toBe(Score.Fourty);
    });

    test('score both getting 40A and one scoring', () => {

        const points = [0, 1, 0, 1, 0, 1, 0, 1]

        let game1 = new TennisGame()
        points.forEach(point => game1.add(point))

        const score = game1.getScore();
        let joueur1 = score[0];
        let joueur2 = score[1];

        expect(joueur1).toBe(Score.Advantage);
        expect(joueur2).toBe(Score.Fourty);
    });

    //
    // test('one player winning a set', () => {
    //
    //     const points = [0, 0, 0, 0]
    //
    //     let game1 = new TennisGame()
    //     points.forEach(point => game1.add(point))
    //
    //     const score = game1.getScore();
    //     let joueur1 = score[0];
    //     let joueur2 = score[1];
    //
    //     const sets = game1.getSets();
    //     let joueur1Winningset = sets[0];
    //     let joueur2Winningset = sets[1];
    //
    //     expect(joueur1).toBe('0');
    //     expect(joueur2).toBe('0');
    //     expect(joueur1Winningset).toBe(1);
    //     expect(joueur1Winningset).toBe(0);
    //
    // });
})
