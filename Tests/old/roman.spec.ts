import {Roman} from "../../src/romain/Roman";

describe('OCR', () => {

    test('Test roman conversion on 1,5,10,50', () => {
        let one = Roman.arabToRoman(1)
        let five = Roman.arabToRoman(5)
        let ten = Roman.arabToRoman(10)
        let fifty = Roman.arabToRoman(50)
        let oneFifty = Roman.arabToRoman(150)

        expect(one).toBe("I");
        expect(five).toBe("V");
        expect(ten).toBe("X");
        expect(fifty).toBe("L");
        expect(oneFifty).toBe("CL");
    });

    test('Test roman conversion on 3,4,9,45', () => {
        let one = Roman.arabToRoman(3)
        let five = Roman.arabToRoman(4)
        let ten = Roman.arabToRoman(9)
        let fifty = Roman.arabToRoman(49)
        let ninety = Roman.arabToRoman(90)
        let ninetyFive = Roman.arabToRoman(95)

        expect(one).toBe("III");
        expect(five).toBe("IV");
        expect(ten).toBe("IX");
        expect(fifty).toBe("XLIX");
        expect(ninety).toBe("XC");
        expect(ninetyFive).toBe("XCV");
    });

});

