import {Player} from "../../src/fight/Player";

describe('Fight Test', () => {

    //ITERATION 1
    //TESTS INITIALISATION
    test('Character heatlth bar', () => {
        let player1 = new Player();
        expect(player1.health).toBe(1000);
    });

    test('Character level', () => {
        let player1 = new Player();
        expect(player1.level).toBe(1);
    });

    test('Character Dead or Alive', () => {
        let player1 = new Player();
        let player2 = new Player();
        player1.levelUp(15);
        player1.attack(player2)
        expect(player1.alive).toBe(true);
        expect(player2.alive).toBe(false);
    });

    //TESTS FIGHTS
    test('Character dealing damage', () => {
        let player1 = new Player();
        let player2 = new Player();
        player1.attack(player2);
        expect(player2.health).toBeLessThan(1000);
    });

    test('Character dealing lethal damage', () => {
        let player1 = new Player();
        let player2 = new Player();
        player1.levelUp(15)
        player1.attack(player2);
        expect(player2.alive).toBe(false);
    });

    test('Character overhealing', () => {
        let player1 = new Player();
        player1.heal(1000000);
        expect(player1.health).toBe(1000);
    });

   //ITERATION TWO
    test('Character cannot Deal Damage to itself', () => {
        let player1 = new Player();
        player1.attack(player1);
        expect(player1.health).toBe(1000);
    });

    test('A Character can only Heal itself', () => {
        let player1 = new Player();
        let player2 = new Player();
        player1.changeHealth(1);
        player2.changeHealth(1);
        player1.heal(50);
        expect(player1.health).toBe(51);
        expect(player2.health).toBe(1);
    });

    test('character fighting with 5 level difference', () => {
        let player1 = new Player();
        let player2 = new Player();
        player1.levelUp(5);
        let damage = player1.damage;
        expect(player2.health).toBeGreaterThan(player2.health - damage);
    });
});

