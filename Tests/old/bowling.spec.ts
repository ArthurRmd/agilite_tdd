import {Game} from "../../src/bowling/Game";

describe('bowling', () => {


    test('Test a single turn casual score', () => {

        const pins = [5,3];
        let game = new Game(pins);
        let score = game.getScore()

        expect(score).toBe(8);
    });

    test('Test a single turn spare', () => {

        const pins = [5,5,6,0];
        let game = new Game(pins);
        let score = game.getScore()

        expect(score).toBe(22);
    });


    test('Test a single turn strike', () => {

        const pins = [1,0,10,7,2];
        let game = new Game(pins);
        let score = game.getScore()

        expect(score).toBe(27);
    });

    test('Fake strike', () => {

        const pins = [0,10,7,2];
        let game = new Game(pins);
        let score = game.getScore()

        expect(score).toBe(26);
    });
})
