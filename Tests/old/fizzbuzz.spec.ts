import {fizzBuzz, fizzBuzzString, replaceFizzBuzz} from "../../src/fizzbuzz/fizzbuzz";
import {fizzBuzzArray} from "../../src/fizzbuzz/fizzbuzz";

describe('bowling', () => {

    test('multiple de 3', () => {

        const number = 3
        let chaine = fizzBuzz(number);
        expect(chaine).toBe("Fizz");
    });


    test('multiple de 5', () => {

        const number = 5
        let chaine = fizzBuzz(number);
        expect(chaine).toBe("Buzz");
    });


    test('multiple de 3 et 5', () => {

        const number = 15
        let chaine = fizzBuzz(number);
        expect(chaine).toBe("FizzBuzz");
    });

    test('multiple de 3 et 5', () => {

        const number = 45
        let chaine = fizzBuzz(number);
        expect(chaine).toBe("FizzBuzz");
    });

    test('autre', () => {

        const number = 14
        let chaine = fizzBuzz(number);
        expect(chaine).toBe(number);
    });


    test('multiple de 5', () => {
        const number = 45
        let chaine = fizzBuzz(number);
        expect(chaine).toBe("FizzBuzz");
    });

    test('test with a string', () => {
        let chaine = fizzBuzzString(30);
        const chaineTest = '1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16 17 Fizz 19 Buzz Fizz 22 23 Fizz Buzz 26 Fizz 28 29 FizzBuzz';
        expect(chaine)
            .toBe(chaineTest);
    });

    test('replace Fizz by Feuzz', () => {
        let chaine = replaceFizzBuzz(fizzBuzzArray(30), 'Fizz', 'Feuzz').join(" ")
        const chaineTest = '1 2 Feuzz 4 Buzz Feuzz 7 8 Feuzz Buzz 11 Feuzz 13 14 FeuzzBuzz 16 17 Feuzz 19 Buzz Feuzz 22 23 Feuzz Buzz 26 Feuzz 28 29 FeuzzBuzz';
        expect(chaine)
            .toBe(chaineTest);
    });



});

