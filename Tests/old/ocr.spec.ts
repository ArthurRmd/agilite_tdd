import {Ocr} from "../../src/ocr/ocr";

describe('OCR', () => {
    test('OCR read data file', () => {

        const ocr = new Ocr()
        let chaine = ocr.readFile('ocr1.txt');
        let number = ocr.convertString(chaine);
        expect(number).toBe(1234567890);
    });

    test('OCR read data file 2 ', () => {

        const ocr = new Ocr()
        let chaine = ocr.readFile('ocr2.txt');
        let number = ocr.convertString(chaine);
        expect(number).toBe(490067715);
    });

    test('OCR read data file 3', () => {

        const ocr = new Ocr()
        let chaine = ocr.readFile('ocr3.txt');
        let number = ocr.convertString(chaine);
        expect(number).toBe(1000000015);
    });

    test('OCR checksum data file 3', () => {

        const ocr = new Ocr()
        let chaine = ocr.readFile('ocr4.txt');
        let number = ocr.convertString(chaine);
        const result = ocr.checkSum(number)
        expect(result).toBe(true);
    });
});

