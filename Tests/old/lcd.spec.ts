import {Lcd} from "../../src/lcd/Lcd";

describe('LCD', () => {

    test('display 0 in LCD', () => {

        const chaine = Lcd.numberToString(0)
        // expect(chaine).toBe(
        //     ' | \n' +
        //             ' | \n' +
        //             ' | \n'
        // );
    });


    test('display 1 in LCD', () => {

        const chaine = Lcd.numberToString(1)
        // expect(chaine).toBe(
        //     '   \n  |\n  |');
    });

    test('display 2 in LCD', () => {

        const chaine = Lcd.numberToString(2)
        // expect(chaine).toBe(
        //     ' _ \n _|\n|_');

    });



    test('display 80 in LCD', () => {

        const chaine = Lcd.numberToString(80)
        expect(chaine).toBe(
            ' ---    ---   \n' +
            '|   |  |   |  \n' +
            ' ---   |   |  \n' +
            '|   |  |   |  \n' +
            ' ---    ---   \n');

    });


});

