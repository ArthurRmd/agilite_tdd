import {Equipment} from "./Equipment";
import {Reserve} from "./Reserve";
import {Position} from "./Enum/Position";
import {Level} from "./Enum/Level";

export class Survivor {

    private _name: string           = ''
    private _wounds: number         = 0
    private _alive: boolean         = true
    private _actions: number        = 3
    private _inHand: Equipment[]    = []
    private _reserve: Reserve
    private _experience :number     = 0
    private _level :Level

    constructor(name: string) {
        this.name = name
        this._reserve = new Reserve()
        this._level = Level.Blue
    }

    addWounds(number: number): void {
        this.wounds += number
        if (this.wounds > 1) {
            this.alive = false
        }
    }

    useAction(): boolean {
        if (this.actions == 0) return false
        this.actions -= 1
        return true
    }

    getHurt(){
        this.addWounds(1);
        this.reserve.reduce()
    }

    addEquipment(equipment: Equipment): boolean {

        if (this.inHand.length < 2) {
            this.inHand.push(equipment)
            return true
        }

        if (this.reserve.equipments.length < 3) {
            this.reserve.equipments.push(equipment)
            return true
        }

        return false
    }

    resetAction() {
        this.actions = 3
    }

    switchEquipment(hand :Position, reservePosition :number) {

        if(hand === Position.Left) {
            if(this.inHand[0]) {
                let equipmentHand = this.inHand[0]
                this.inHand[0] =this.reserve.equipments[reservePosition]
                this.reserve.equipments[reservePosition] = equipmentHand
            }

        }

        if(hand === Position.Right) {
            if(this.inHand[1]) {
                let equipmentHand = this.inHand[1]
                this.inHand[1] =this.reserve.equipments[reservePosition]
                this.reserve.equipments[reservePosition] = equipmentHand
            }
        }
    }

    /* Getter | Setter */

    get name(): string {
        return <string>this._name;
    }

    get wounds(): number {
        return this._wounds;
    }

    set wounds(value: number) {
        this._wounds = value;
    }

    get alive(): boolean {
        return this._alive;
    }

    get actions(): number {
        return this._actions;
    }

    set actions(value: number) {
        this._actions = value;
    }

    set alive(value: boolean) {
        this._alive = value;
    }

    set name(value: string) {
        this._name = value;
    }

    get reserveSize(): number {
        return this.reserve.capacity;
    }

    get reserve() :Reserve {
        return this._reserve;
    }

    get inHand(): Equipment[] {
        return this._inHand;
    }

    getHand(position : Position): Equipment {
        return this._inHand[Position.Left == position ?0:1];
    }

    getReserveEquipment(position:number) :Equipment{
        return this.reserve.equipments[position]
    }

    get level(): Level {
        return this._level;
    }

    get experience(): Level {
        return this._experience;
    }}
