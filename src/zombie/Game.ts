import {Survivor} from "./Survivor";

export class Game {

    private survivors :Survivor[] = []
    private _turn :number = 1
    private _isRunning :boolean = true

    constructor() {}

    addSurvivor( newSurvivor :Survivor) :boolean {

        let findSurvivorWithSameName = false
        this.survivors.forEach(survivor => {
            if(newSurvivor.name === survivor.name)
                findSurvivorWithSameName = true
        })

        if (!findSurvivorWithSameName)
            this.survivors.push(newSurvivor);

        return !findSurvivorWithSameName;
    }


    changeTurn() :boolean{
        let canTurn = true
        this.survivors.forEach(survivor => {
            if (survivor.actions > 0) canTurn = false
        })

        if(canTurn) {
            this._turn ++
            this.survivors.forEach(survivor => {
                survivor.resetAction()
            })
        }

        return canTurn
    }

   isRunning(): boolean {

       this._isRunning = false
        if(!this.survivors.length) this._isRunning = true

        this.survivors.forEach(survivor => {
            if (survivor.alive)
                this._isRunning = true
        })

        return this._isRunning
   }

    get turn(): number {
        return this._turn;
    }

}
