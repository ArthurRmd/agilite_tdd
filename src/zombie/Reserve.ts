import {Equipment} from "./Equipment";

export class Reserve {

    private _equipments :Equipment[] = []
    private _capacity :number = 3

    constructor() {}

    reduce() {
        this.capacity -= 1
        if (this.equipments.length > this.capacity) {
            this.equipments.shift()
        }
    }

    get capacity(): number {
        return this._capacity;
    }

    set capacity(value: number) {
        this._capacity = value;
    }
    get equipments(): Equipment[] {
        return this._equipments;
    }

}
