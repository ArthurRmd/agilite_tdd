export class Game {

    shots: number[]

    constructor(shots: number[]) {
        this.shots = shots
    }


    getScore(): number {
        let somme = 0;


        for (let i = 0; i < this.shots.length; i++) {
            const shot = this.shots[i]
            if (shot === 10) {
                // strike
                somme= somme + 10 + this.shots[i+1] + this.shots[i+2]
            }
            else if(i < this.shots.length && shot + this.shots[i+1] === 10) {
                // spare
                i+=1
                somme= somme + 10 + this.shots[i+1]
            }
            else {
                somme += shot
            }



        }

        return somme
    }
}
