import {LcdBuilder} from "./LcdBuilder";
import {log} from "util";

export class Lcd {


    static numberToString(number: number): string {

        let builder = new LcdBuilder()
        let numberString = number.toString()
        let numbers :string[] = []

        for(let i = 0; i < numberString.length; i++) {
            let tempNumber = parseInt(numberString[i])

            switch (tempNumber) {

                case 0:
                    builder
                        .addLine()
                        .addBothPipe()
                        .addBothPipe()
                        .addBothPipe()
                        .addLine()
                    break

                case 8:
                    builder
                        .addLine()
                        .addBothPipe()
                        .addLine()
                        .addBothPipe()
                        .addLine()
                    break


            }

            numbers.push(builder.build())
        }

        for (let i = 0; i < numbers.length - 1; i++) {
            for (let j = 0; j < 6; j++)
                numbers[i] = numbers[i].replace(' \n', '  @\n')

            let nextNumber = numbers[i+1].split('\n')
            nextNumber.forEach(number=> {
                numbers[i] = numbers[i].replace('@', number + ' ')
            })


        }

        return numbers[0]

    }

}

