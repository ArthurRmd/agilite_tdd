

export class LcdBuilder {

    number = ''

    constructor() {}

    build() :string {
        let value = this.number
        this.number = ''
        return value
    }

    addLine() :LcdBuilder{
        this.number +=' --- \n'
        return this
    }

    addBothPipe() :LcdBuilder{
        this.number +='|   | \n'
        return this
    }

    addPipe2() :LcdBuilder{
        this.number +='   | \n'
        return this
    }

    addPipe1() :LcdBuilder{
        this.number +='|    \n'
        return this
    }
}
