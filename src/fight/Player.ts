
import { v4 as uuidv4 } from 'uuid';

export class Player {

    private _maxHeal = 1000
    private _health = 0
    private _level = 1
    private _alive = true
    private _damage = 100
    private _uuid = ''

    constructor() {
        this._health = this._maxHeal
        this._uuid = uuidv4()
    }

    public receiveDamage(value:number) :void {
        this.changeHealth(this._health - value)
    }

    public heal(value:number) :void {
        this.changeHealth(this._health + value)
    }

    public attack(player:Player) :void {
        if (this._uuid === player._uuid) return
        let damage = this.damage;
        if (player.level + 5 >= this.level) damage /= 2
        if (player.level - 5 >= this.level) damage *= 2
        player.receiveDamage(damage)
    }

    changeHealth(value:number):void {
        if (value < 0) value = 0
        if (value >= this._maxHeal) value = this._maxHeal
        if (value === 0 ) this._alive = false
        this._health = value
    }

    levelUp(level: number) :void {
        this._level += level
        this._damage *= 1.1 * level
        this._health += (0.5 * this.health) * level
    }

    get health(): number {
        return this._health;
    }

    get level(): number {
        return this._level;
    }

    get alive(): boolean {
        return this._alive;
    }

    get damage(): number {
        return this._damage;
    }

}
