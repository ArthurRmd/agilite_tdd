

export function fizzBuzz(number : number) :string|number{

    if (number % 3 == 0 && number % 5 == 0) {
        return "FizzBuzz"
    }
    else if (number % 3 == 0) {
        return "Fizz"
    }
    else if (number % 5 == 0) {
        return "Buzz"
    }
    else {
        return number
    }
}

export function replaceFizzBuzz(array:(string|number)[], from: string, to: string) :(string|number)[] {
    const newValues :(string|number)[] = []

    array.map(element => {
        if(typeof element === "string" && element.includes(from))
            element = element.replace(from, to)
        newValues.push(element)
    })
    return newValues
}

export function fizzBuzzArray(number : number) :(number|string)[]{

    let values :(number|string)[] = []

    for (let i = 1; i <= number ; i++) {
        values.push(fizzBuzz(i))
    }

    return values
}

export function fizzBuzzString(number : number) :string{
    return fizzBuzzArray(number).join(' ')
}
