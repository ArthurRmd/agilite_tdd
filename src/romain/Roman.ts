import {mappingNumber, RomanNumber} from "./convert";

export class Roman {


    static arabToRoman(number:number) :string {

        if(number === 0) return ''

        let romanNumber :RomanNumber = {
            arab:0,
            roman:''
        }
        let allRomanNumber = mappingNumber()

        while(romanNumber.arab < number ) {
            for (let i = 0; i < allRomanNumber.length; i++) {

                if((romanNumber.arab + allRomanNumber[i].value) <= number) {
                    romanNumber.arab += allRomanNumber[i].value
                    romanNumber.roman += allRomanNumber[i].chaine
                    break
                }
            }

        }

        return romanNumber.roman
    }



}
