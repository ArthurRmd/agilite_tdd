type Number = {
    value:number,
    chaine:string
}

export type RomanNumber = {
    arab:number,
    roman:string
}
export function mappingNumber() :Number[] {

    const values :Number[] = [
        {
            value:1000,
            chaine:
                "M"
        },
        {
            value:900,
            chaine:
                "CM"
        },
        {
            value:500,
            chaine:
                "D"
        },
        {
            value:400,
            chaine:
                "CD"
        },
        {
            value:100,
            chaine:
                "C"
        },
        {
            value:90,
            chaine:
                "XC"
        },
        {
            value:50,
            chaine:
                "L"
        },
        {
            value:40,
            chaine:
                "XL"
        },
        {
            value:10,
            chaine:
                "X"
        },
        {
            value:9,
            chaine:
                "IX"
        },
        {
            value:5,
            chaine:
                "V"
        },
        {
            value:4,
            chaine:
                "IV"
        },
        {
            value:1,
            chaine:
                "I"
        },
    ]

    return values
}
