import {Game} from "./zombie/Game";
import {Survivor} from "./zombie/Survivor";

let game1 = new Game()

let survivor1 = new Survivor('Arthur')
let survivor2 = new Survivor('Quentin')

// console.log(game1.isRunning())

game1.addSurvivor(survivor1)
game1.addSurvivor(survivor2)

console.log(game1.isRunning())

survivor1.getHurt()
survivor1.getHurt()
// console.log(game1.isRunning())
survivor2.getHurt()
survivor2.getHurt()
// console.log(game1.isRunning())
