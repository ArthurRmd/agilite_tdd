import {Score} from "./Score";

export class TennisGame {

    points: number[]

    constructor() {
        this.points = []
    }

    add(point: number) {
        this.points.push(point)
    }

    getScore(): number[] {

        let player1 = Score.Zero
        let player2 = Score.Zero


        this.points.forEach(game => {


            if (game != 1) {
                const temp = this.increasePlayerPoints(player1, player2)
                player1 = temp[0]
                player2 = temp[1]
            } else {
                const temp = this.increasePlayerPoints(player2, player1)
                player1 = temp[1]
                player2 = temp[0]
            }

        })

        return [player1, player2]
    }


    private increasePlayerPoints(playerReceivePoint: Score, otherPlayer: Score) {


        if (playerReceivePoint === Score.Zero) {
            playerReceivePoint = Score.Fifteen
        }

        else if (playerReceivePoint === Score.Fifteen) {
            playerReceivePoint = Score.Thirty
        }

        else if (playerReceivePoint === Score.Thirty && otherPlayer < 3) {
            playerReceivePoint = Score.Fourty
        }

        else if (playerReceivePoint === Score.Thirty && otherPlayer === Score.Fourty) {
            playerReceivePoint = Score.FourtyAdvantage
            otherPlayer = Score.FourtyAdvantage
        }

        else if (playerReceivePoint === Score.FourtyAdvantage&& otherPlayer === Score.FourtyAdvantage) {
            playerReceivePoint = Score.Advantage
            otherPlayer = Score.Fourty
        }

        return [playerReceivePoint, otherPlayer]
    }

    getSets() {
        return [1, 0]
    }
}
