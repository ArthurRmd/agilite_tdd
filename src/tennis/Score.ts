
export enum Score {
    Zero,
    Fifteen,
    Thirty,
    Fourty,
    FourtyAdvantage,
    Advantage,
    Win
}
