import * as fs from "fs";
import {mappingNumber} from "./convert";


export class Ocr {

    constructor() {
    }

    readFile(filename: string) {

        let path = '/home/arthur/dev/cours/agilite_tdd/data/' + filename
        const content = fs.readFileSync(path, 'utf8');
        let result: string[] = []

        const lines = content.split('\n')

        lines.forEach(line => {
            let j = 0
            for (let i = 0; i < line.length; i = i + 3) {
                result[j] = result[j]
                    ? result[j] + line[i] + line[i + 1] + (line[i + 2] || " ") + '\n'
                    : line[i] + line[i + 1] + (line[i + 2] || " ") + '\n'
                j++
            }
        })

        return result;
    }

    convert(numberString: string): number {
        let numberArray = mappingNumber();
        return <number>numberArray.find(obj => obj.chaine == numberString)?.value;
    }


    convertString(arrayOfString: string[]): number {
        let finalNumber = '';
        arrayOfString.forEach(num => {
            finalNumber += this.convert(num).toString()
        })

        return parseInt(finalNumber);
    }

    checkSum(checksumCode: number): boolean {

        let reverse = (checksumCode:number) =>
            parseInt(
                String(checksumCode)
                    .split("")
                    .reverse()
                    .join("")
                , 10
            )

        let number = reverse(checksumCode).toString()
        let sum = 1

        for(let i = 0; i < number.length ; i++)
            sum *= (parseInt(number[i]) + i+2)

        return (sum % 11 === 0);
    }
}
