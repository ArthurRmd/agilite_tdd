type Number = {
    value:number,
    chaine:string
}

export function mappingNumber() :Number[] {

    const values :Number[] = [
        {
            value:0,
            chaine:
                " _ \n" +
                "| |\n" +
                "|_|\n"
        },
        {
            value:1,
            chaine:
                "   \n" +
                "  |\n" +
                "  |\n"
        },
        {
            value:2,
            chaine:
                " _ \n" +
                " _|\n" +
                "|_ \n"
        },
        {
            value:3,
            chaine:
                " _ \n" +
                " _|\n" +
                " _|\n"
        },
        {
            value:4,
            chaine:
                "   \n" +
                "|_|\n" +
                "  |\n"
        },
        {
            value:5,
            chaine:
                " _ \n" +
                "|_ \n" +
                " _|\n"
        },
        {
            value:6,
            chaine:
                " _ \n" +
                "|_ \n" +
                "|_|\n"
        },
        {
            value:7,
            chaine:
                " _ \n" +
                "  |\n" +
                "  |\n"
        },
        {
            value:8,
            chaine:
                " _ \n" +
                "|_|\n" +
                "|_|\n"
        },
        {
            value:9,
            chaine:
                " _ \n" +
                "|_|\n" +
                " _|\n"
        }
    ]

    return values

}
